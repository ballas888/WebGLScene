/**
 * Created by Ballas on 12/12/2015.
 */

var Input = function(type){
    var date = new Date();
    this.id = Math.random()*date.getMilliseconds();
    this.type = type;
};

function Keyboard(){
    Input.call(this,"Keyboard");
    var _ = this;

    _.keys = [];
    for(var i = 0; i < 255; i++){
        _.keys[i] = false;
    }

    _.keyW = false;
    _.keyS = false;
    _.keyA = false;
    _.keyD = false;

    this.keyDown = function(event){
        _.keys[event.keyCode] = true;
    };
    this.heyUp = function(event){
        _.keys[event.keyCode] = false;
    };
}

Keyboard.prototype = Object.create(Input.prototype);
Keyboard.prototype.constructor = Keyboard;
Keyboard.prototype.update = function(){
    var _ = this;
    var w = 87;
    var s = 83;
    var a = 65;
    var d = 68;

    _.keyW = _.keys[w];

    _.keyS = _.keys[s];

    _.keyA = _.keys[a];

    _.keyD = _.keys[d];

};

function Mouse(){
    Input.call(this,"Mouse");

    var _ = this;

    this.mouseDown = function(event){
        console.log(event);
    };
    this.mouseUp = function(event){

    };
}

Mouse.prototype = Object.create(Input.prototype);
Mouse.prototype.constructor = Mouse;
