/**
 * Created by Ballas on 12/14/2015.
 */

var Loader = {
    fileTypes: {
        obj: "obj",
        json: "json"
    }
};

Loader.load = function(type, location, gl){
    var _ = this;
    if(!(type in _.fileTypes)){
        alert("File Type Does NOT Compute: Loader.load");
        return null;
    }
    //console.log(gl);
    var obj = null;

    var loaded = null;

    if(_.fileTypes[type] === "obj"){
        obj = new Mesh();
        loaded = function(event){
            var data = event.target.responseText;
            Loader.loadOBJ(data, obj);
            obj.init(gl);
        }
    }

    var client = new XMLHttpRequest();
    client.addEventListener("load", loaded);
    client.open('GET', location);
    client.send();

    return obj;

    //var client = new XMLHttpRequest();
    //client.addEventListener("load", loaded);
    ////client.addEventListener("progress", progress);
    //client.open('GET', location);
    //client.send();
    //
    //function loaded(event){
    //    var data = event.target.responseText;
    //    switch(type){
    //        case Dixy.fileTypes[0]:
    //            obj =
    //
    //            Loader.loadOBJ(data, mesh);
    //            console.log(Dixy.gl);
    //            mesh.init(Dixy.gl);
    //            break;
    //        case Dixy.fileTypes[1]:
    //
    //            break;
    //        default:
    //
    //            break;
    //
    //    }
    //}
    //
    //
    //return obj;
};

Loader.loadOBJ = function(input, mesh){

    var vArray = [];
    var nArray = [];
    var tArray = [];
    var unpacked = {
        v: [],
        n: [],
        t: [],
        i: [],
        hash: {},
        index: 0
    };

    var v_reg = /^v\s/;
    var n_reg = /^vn\s/;
    var t_reg = /^vt\s/;
    var f_reg = /^f\s/;
    var space_reg = /\s+/;

    var lines = input.split('\n');
    for(var lineCount = 0; lineCount < lines.length; lineCount++){
        var line = lines[lineCount].trim();
        var elements = line.split(space_reg);
        elements.shift();

        if(v_reg.test(line)){
            vArray.push.apply(vArray, elements);
        }
        else if(n_reg.test(line)){
            nArray.push.apply(nArray, elements);
        }
        else if(t_reg.test(line)){
            tArray.push.apply(tArray, elements);
        }
        else if(f_reg.test(line)){

            for(var j = 0, eLen = elements.length; j < eLen; j++){
                var elem = elements[j];

                if(elem in unpacked.hash){
                    unpacked.i.push(unpacked.hash[elem]);
                }else{
                    var vertex = elem.split('/');

                    unpacked.v.push(+vArray[(vertex[0] - 1) * 3]);
                    unpacked.v.push(+vArray[(vertex[0] - 1) * 3+1]);
                    unpacked.v.push(+vArray[(vertex[0] - 1) * 3+2]);

                    if(tArray.length){
                        unpacked.t.push(+tArray[(vertex[1]-1)*2]);
                        unpacked.t.push(+tArray[(vertex[1]-1)*2+1]);
                    }

                    unpacked.n.push(+nArray[(vertex[2]-1)*3]);
                    unpacked.n.push(+nArray[(vertex[2]-1)*3+1]);
                    unpacked.n.push(+nArray[(vertex[2]-1)*3+2]);

                    unpacked.hash[elem] = unpacked.index;
                    unpacked.i.push(unpacked.index);

                    unpacked.index++;
                }
            }
        }
    }
    mesh.v.data = unpacked.v;
    mesh.n.data = unpacked.n;
    mesh.t.data = unpacked.t;
    mesh.i.data = unpacked.i;

    return mesh;
};