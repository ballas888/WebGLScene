/**
 * Created by Ballas on 12/14/2015.
 */

var Mesh = function(){
    var _ = this;
    _.v= {buffer: null, data:[], size: 3};
    _.n= {buffer: null, data:[], size: 3};
    _.c= {buffer: null, data:[], size: 3};
    _.t= {buffer: null, data:[], size: 2};
    _.i= {buffer: null, data:[], size: 3};
    _.isReady= false;
    _.isLine = false;
    _.matrix= mat4.create();
};

Mesh.load = function(type,location,gl){
    return Loader.load(type,location,gl);
};

Mesh.prototype.rotate = function(a,axis){
    var _ = this;
    mat4.rotate(_.matrix, _.matrix,a,axis);
};

Mesh.prototype.translate = function(vec){
    var _ = this;
    mat4.translate(_.matrix, _.matrix, vec)
};

Mesh.prototype.init = function(gl){
    var _ = this;

    _.v.buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, _.v.buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(_.v.data),gl.STATIC_DRAW);

    if(_.n.data.length){
        _.n.buffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, _.n.buffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(_.n.data),gl.STATIC_DRAW);
    }

    if(_.c.data.length){
        _.c.buffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, _.c.buffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(_.c.data),gl.STATIC_DRAW);
    }

    if(_.t.data.length){
        _.t.buffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, _.t.buffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(_.t.data),gl.STATIC_DRAW);
    }

    _.i.buffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, _.i.buffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(_.i.data),gl.STATIC_DRAW);

    gl.bindBuffer(gl.ARRAY_BUFFER,null);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,null);

    _.isReady = true;
};

Mesh.prototype.draw = function(gl,shader,options){
    var _ = this;
    if(_.isReady){

        gl.uniformMatrix4fv(shader.unif["u_Mmatrix"], false, _.matrix);

        gl.bindBuffer(gl.ARRAY_BUFFER, _.v.buffer);
        gl.vertexAttribPointer(shader.attr["a_Position"], _.v.size, gl.FLOAT, false, 0, 0);

        if(_.c.data.length){
            gl.bindBuffer(gl.ARRAY_BUFFER, _.c.buffer);
            gl.vertexAttribPointer(shader.attr["a_Color"], _.c.size, gl.FLOAT, false, 0, 0);
        }else if(_.n.data.length){
            gl.bindBuffer(gl.ARRAY_BUFFER, _.n.buffer);
            gl.vertexAttribPointer(shader.attr["a_Normal"], _.n.size, gl.FLOAT, false, 0, 0);
        }

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, _.i.buffer);
        if(_.isLine){
            gl.drawElements(gl.LINES,  _.i.data.length, gl.UNSIGNED_SHORT, 0);
        }else{

            gl.drawElements(gl.TRIANGLES,  _.i.data.length, gl.UNSIGNED_SHORT, 0);
        }

    }
};

Mesh.prototype.destroy = function(gl){

};