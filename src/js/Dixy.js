/**
 * Created by Ballas on 12/14/2015.
 */

var Dixy = Dixy || {};

Dixy.create = (function(canvas, updateCallback, renderCallback){
    var _ = this;

    _.startTime = null;
    _.deltaTime = 0;
    _.prevTime = 0;
    _.loopTick = 0;
    _.isRunning = false;
    _.currentFps = 0;

    _.gl = null;
    _.canvas = canvas;

    try{
        _.gl = _.canvas.getContext("webgl",{antialias: true}) ||
            _.canvas.getContext("experimental-webgl",{antialias: true});

        //_.gl = WebGLDebugUtils.makeDebugContext(_.gl , _.throwOnGLError, _.logAndValidate);
        //WebGLDebugUtils.init(_.gl);
        //
        _.gl.clearColor(0.3, 0.3, 0.3, 1.0);
        _.gl.enable(_.gl.CULL_FACE)
        _.gl.enable(_.gl.DEPTH_TEST);
        //_.gl.cullFace(_.gl.BACK);
        _.gl.depthFunc(_.gl.LEQUAL);
        _.gl.clear(_.gl.COLOR_BUFFER_BIT | _.gl.DEPTH_BUFFER_BIT);

    }catch(e){
        console.log(e);
        alert(e+"\nUnable to initialize WebGL. Your browser may not support it.");
        _.gl = null;
    }

    _.loop = function(time, isStart){
        if(!_.startTime){
            _.startTime = time;
            requestAnimationFrame(function(time){
                _.loop(time, false);
            });
            return;
        }

        if(isStart){
            _.prevTime = time;
            requestAnimationFrame(function(time){
                _.loop(time, false);
            });
            return;
        }

        if(_.isRunning){
            _.deltaTime = time - _.prevTime;
            _.currentFps = 1000/ _.deltaTime;
            _.prevTime = time;
            _.update(time, _.deltaTime, _.loopTick);
            _.render(time, _.deltaTime, _.loopTick);
            _.loopTick++;
            requestAnimationFrame(function(time){
                _.loop(time,false);
            });
        }
    };

    _.update = function(time,delta,tick){
        updateCallback(time,delta,tick);
    };
    _.render = function(time,delta,tick){
        _.gl.clear(_.gl.COLOR_BUFFER_BIT | _.gl.DEPTH_BUFFER_BIT);
        renderCallback(time,delta,tick);
        _.gl.flush();
    };

    return {
        start: function(){
            _.isRunning = true;
            requestAnimationFrame(function(time){
                _.loop(time,true);
            });
        },
        stop: function(){
            _.isRunning = false;
        },
        isRunning: function(){
            return _.isRunning;
        },
        currentFps: function(){
            return _.currentFps;
        },
        getGL: function(){
            return _.gl;
        },
        setViewport: function(x,y,width,height){
            _.gl.viewport(x, y, width, height);
        }
    }
});

Dixy.createGrid = (function(gridSize){
    var _ = this;
    var grid = new Mesh();
    grid.isLine = true;

    var gridMid = (gridSize -1)/2*-1;
    var gridMidInv = gridMid*-1;
    var x = gridMid;
    var z = gridMid;
    var tempArray = [];
    for(var i = 0; i< gridSize; i++){
        for(var j = 0; j < gridSize; j++){
            tempArray[(i*gridSize)+j] = {
                x: x,
                z: z
            };
            z++;
        }
        z = gridMid;
        x++;
    }

    for(var i =0; i < tempArray.length; i++){
        grid.v.data.push(tempArray[i].x);
        grid.v.data.push(0);
        grid.v.data.push(tempArray[i].z);

        if(i == gridMidInv || i == (gridSize*gridSize-(gridMidInv+1))){
            grid.c.data.push(0.0);
            grid.c.data.push(0.6);
            grid.c.data.push(0.0);
        }
        else if(i == (gridSize*gridMidInv) || i==(gridSize*gridMidInv+(gridSize-1))){
            grid.c.data.push(0.6);
            grid.c.data.push(0.0);
            grid.c.data.push(0.0);
        }else{
            grid.c.data.push(0.45);
            grid.c.data.push(0.45);
            grid.c.data.push(0.45);
        }

    }

    for(var i = 0; i < gridSize; i++){
        for(var j = 0; j<2; j++){
            var a = i*(gridSize);
            var b = a+(gridSize)-1;
            grid.i.data.push(a);
            grid.i.data.push(b);
        }
    }

    for(var i = 0; i < gridSize; i++){
        for(var j = 0; j<2; j++){
            var a = i;
            var b = (gridSize*gridSize)-(gridSize-i);
            //console.log(a+" "+b);
            grid.i.data.push(a);
            grid.i.data.push(b);
        }
    }

    grid.init(_.gl);
    return grid;
});

//Dixy.create = (function(FRAME_PER_SECOND, CANVAS, DRAW_FUNC, UPDATE_FUNC, OPTIONS){
//    var _ = this;
//
//    _.MAX_REFRESH_RATE = (1000/2/FRAME_PER_SECOND);
//    _.startTime = new Date().getTime();
//    _.skipMilli = (1000/FRAME_PER_SECOND);
//    _.isRunning = false;
//    _.nextTick = 0;
//    _.lastPausedAt = 0;
//    _.allPausedTime = 0;
//    _.hasGrid = false;
//
//    if(OPTIONS.gridSize && OPTIONS.gridSize > 0 && !(OPTIONS.gridSize%2 == 0)){
//        _.gridSize = OPTIONS.gridSize;
//        _.hasGrid = true;
//        _.grid = new Mesh();
//        _.grid.isLine = true;
//
//        var gridSize = _.gridSize;
//        var gridMid = (gridSize -1)/2*-1;
//        var gridMidInv = gridMid*-1;
//        var x = gridMid;
//        var z = gridMid;
//        var tempArray = [];
//        for(var i = 0; i< gridSize; i++){
//            for(var j = 0; j < gridSize; j++){
//                tempArray[(i*gridSize)+j] = {
//                    x: x,
//                    z: z
//                };
//                z++;
//            }
//            z = gridMid;
//            x++;
//        }
//
//        for(var i =0; i < tempArray.length; i++){
//            _.grid.v.data.push(tempArray[i].x);
//            _.grid.v.data.push(0);
//            _.grid.v.data.push(tempArray[i].z);
//
//            if(i == gridMidInv || i == (gridSize*gridSize-(gridMidInv+1))){
//                _.grid.c.data.push(0.0);
//                _.grid.c.data.push(0.6);
//                _.grid.c.data.push(0.0);
//            }
//            else if(i == (gridSize*gridMidInv) || i==(gridSize*gridMidInv+(gridSize-1))){
//                _.grid.c.data.push(0.6);
//                _.grid.c.data.push(0.0);
//                _.grid.c.data.push(0.0);
//            }else{
//                _.grid.c.data.push(0.45);
//                _.grid.c.data.push(0.45);
//                _.grid.c.data.push(0.45);
//            }
//
//        }
//
//        for(var i = 0; i < gridSize; i++){
//            for(var j = 0; j<2; j++){
//                var a = i*(gridSize);
//                var b = a+(gridSize)-1;
//                _.grid.i.data.push(a);
//                _.grid.i.data.push(b);
//            }
//        }
//
//        for(var i = 0; i < gridSize; i++){
//            for(var j = 0; j<2; j++){
//                var a = i;
//                var b = (gridSize*gridSize)-(gridSize-i);
//                //console.log(a+" "+b);
//                _.grid.i.data.push(a);
//                _.grid.i.data.push(b);
//            }
//        }
//
//    }
//
//    _.gl = null;
//    _.canvas = CANVAS;
//    _.width = 0;
//    _.height = 0;
//
//    _.throwOnGLError = function(err, funcName, args){
//        throw WebGLDebugUtils.glEnumToString(err) + " was caused by call to: " + funcName;
//    };
//
//    _.logAndValidate = function(functionName, args) {
//        //_.logGLCall(functionName, args);
//        _.validateNoneOfTheArgsAreUndefined (functionName, args);
//    };
//
//    _.logGLCall = function(functionName, args) {
//        console.log("gl." + functionName + "(" +
//            WebGLDebugUtils.glFunctionArgsToString(functionName, args) + ")");
//    };
//
//    _.validateNoneOfTheArgsAreUndefined = function(functionName, args) {
//        for (var ii = 0; ii < args.length; ++ii) {
//            if (args[ii] === undefined) {
//                console.error("undefined passed to gl." + functionName + "(" +
//                    WebGLDebugUtils.glFunctionArgsToString(functionName, args) + ")");
//            }
//        }
//    };
//
//    try{
//        _.gl = _.canvas.getContext("webgl",{antialias: true}) ||
//            _.canvas.getContext("experimental-webgl",{antialias: true});
//
//        _.gl = WebGLDebugUtils.makeDebugContext(_.gl , _.throwOnGLError, _.logAndValidate);
//        WebGLDebugUtils.init(_.gl);
//        //
//        _.gl.clearColor(0.3, 0.3, 0.3, 1.0);
//        _.gl.enable(_.gl.DEPTH_TEST);
//        _.gl.cullFace(_.gl.BACK);
//        _.gl.depthFunc(_.gl.LEQUAL);
//        _.gl.clear(_.gl.COLOR_BUFFER_BIT | _.gl.DEPTH_BUFFER_BIT);
//
//    }catch(e){
//        console.log(e);
//        alert(e+"\nUnable to initialize WebGL. Your browser may not support it.");
//        _.gl = null;
//    }
//
//    _.loop = function(){
//        _.pauseCount();
//
//        if(_.isRunning){
//            var startTick = _.startTime -
//                _.allPausedTime;
//            var currentTick = new Date().getTime() - startTick;
//
//            if(currentTick > _.nextTick){
//                _.countRealFPS();
//                _.update(currentTick);
//                _.draw(currentTick);
//                _.nextTick = _.skipMilli *
//                    (Math.floor(currentTick/_.skipMilli)+1);
//            }
//
//            setTimeout(function(){
//                _.loop();
//            },_.MAX_REFRESH_RATE);
//        }
//    };
//
//    _.pauseCount = function(){
//        if(_.isRunning && _.lastPausedAt){
//            var lastPausedDuration = new Date().getTime() - _.lastPausedAt;
//            if(lastPausedDuration){
//                _.allPausedTime += lastPausedDuration;
//            }
//            _.lastPausedAt = 0;
//        }
//
//        if(!_.isRunning && !_.lastPausedAt){
//            _.lastPausedAt = new Date().getTime();
//        }
//    };
//
//    _.countRealFPS = function(){
//        var currentSecond = (Math.floor(new Date().getTime()/1000));
//        if(_.realFpsSecond == currentSecond){
//            _.realFpsTicks++;
//        }else{
//            _.realFpsCurrentValue = _.realFpsTicks;
//            _.realFpsTicks = 0;
//        }
//        _.realFpsSecond = currentSecond;
//    };
//
//    _.update= function(currentGameTick){
//        _.gl.clear(_.gl.COLOR_BUFFER_BIT | _.gl.DEPTH_BUFFER_BIT);
//        UPDATE_FUNC(currentGameTick);
//        _.gl.flush();
//    };
//
//    _.draw = function(currentGameTick){
//        DRAW_FUNC(currentGameTick);
//    };
//
//    if(_.hasGrid){
//        _.grid.init(_.gl);
//    }
//
//    return{
//        start: function(){
//            if(!_.ID){
//                _.ID = Math.floor(Math.random()*1000000);
//            }
//            if(!_.isRunning){
//                _.isRunning = true;
//                _.loop();
//            }
//            return _.ID;
//        },
//
//        stop: function(){
//            _.isRunning = false;
//            _.loop();
//        },
//
//        getRealFps: function(){
//            return _.realFpsCurrentValue;
//        },
//
//        isRunning: function(){
//            return _.isRunning;
//        },
//
//        setSize: function(w,h){
//            _.width = w;
//            _.height = h;
//            _.canvas.width = w;
//            _.canvas.height = h;
//            _.gl.viewport(0.0, 0.0, w, h);
//        },
//
//        getSize: function(){
//            return {width: _.canvas.width, height: _.canvas.height};
//        },
//
//        getGL: function(){
//            return _.gl;
//        },
//
//        drawGrid: function(gl, attributes){
//            if(_.hasGrid){
//                _.grid.draw(gl,attributes);
//            }
//        }
//    };
//
//});

Dixy.createShaderProgram = (function(vertId, fragId){
    var _ = this;

    _.getShader = function(id){
        var shaderScript, theSource, currentChild, shader;
        shaderScript = document.getElementById(id);

        if (!shaderScript) {
            return null;
        }

        theSource = "";
        currentChild = shaderScript.firstChild;

        while(currentChild) {
            if (currentChild.nodeType == currentChild.TEXT_NODE) {
                theSource += currentChild.textContent;
            }

            currentChild = currentChild.nextSibling;
        }

        //console.log(theSource);

        if (shaderScript.type == "x-shader/x-fragment") {
            shader = _.gl.createShader(_.gl.FRAGMENT_SHADER);
        } else if (shaderScript.type == "x-shader/x-vertex") {
            shader = _.gl.createShader(_.gl.VERTEX_SHADER);
        } else {
            // Unknown shader type
            return null;
        }

        _.gl.shaderSource(shader, theSource);

        // Compile the shader program
        _.gl.compileShader(shader);

        // See if it compiled successfully
        if (!_.gl.getShaderParameter(shader, _.gl.COMPILE_STATUS)) {
            alert("An error occurred compiling the shaders: " + _.gl.getShaderInfoLog(shader) +" : "+id);
            return null;
        }
        return shader;
    };

    var vertShader = _.getShader(vertId);
    var fragShader = _.getShader(fragId);

    var shaderProgram = _.gl.createProgram();
    _.gl.attachShader(shaderProgram, vertShader);
    _.gl.attachShader(shaderProgram, fragShader);
    _.gl.linkProgram(shaderProgram);

    // If creating the shader program failed, alert
    if (!_.gl.getProgramParameter(shaderProgram, _.gl.LINK_STATUS)) {
        alert("Unable to initialize the shader program.");
    }

    return {
        program: shaderProgram,
        vert: vertShader,
        frag: fragShader,
        attributes: {},
        uniforms: {}
    };
});



