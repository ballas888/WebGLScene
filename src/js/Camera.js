/**
 * Created by Ballas on 12/7/2015.
 */

function Camera(vec3P, angleV, angleH){
    this.vec3P = vec3P;
    this.vec3D = vec3.create();
    this.vec3U = vec3.create();
    this.vec3R = vec3.create();
    this.vec3T = vec3.create();
    this.matrixRotate = mat4.create();
    this.angleV = angleV;
    this.angleH = angleH;
    this.matrix = mat4.create();
    this.matrixProj = mat4.create();
    this.matrixView = mat4.create();
    this.matrixCamera = mat4.create();
    this.qY = quat.create();




    this.update();

}

Camera.prototype.setToPerspective = function(fov, ratio, near, far){
    mat4.perspective(this.matrixProj,fov,ratio,near,far);
};

Camera.prototype.setToOrtho = function(left, right, bottom, top, near, far){
    mat4.ortho(this.matrixProj, left, right, bottom, top, near, far)
};

Camera.prototype.setAngleRVertical = function(angleV){
    this.angleV = angleV;
};

Camera.prototype.setAngleRHorizontal = function(angleH){
    this.angleH = angleH;
};

Camera.prototype.setAngleDVertical = function(angleV){

    this.angleV = (angleV/180)*Math.PI;
};

Camera.prototype.setAngleDHorizontal = function(angleH){
    this.angleH = (angleH/180)*Math.PI;
};

Camera.prototype.getAngleRVertical = function(){
    return this.angleV;
};

Camera.prototype.getAngleRHorizontal = function(){
    return this.angleH;
};

Camera.prototype.translate = function(axis){
    var _ = this;

    //_.vec3P = axis;

    vec3.add(_.vec3P, _.vec3P,axis);

    //mat4.lookAt(_.matrixView, _.vec3P, _.vec3T, _.vec3U);
    //mat4.multiply(_.matrixView, _.matrixView,_.matrixRotate);
};

Camera.prototype.update = function(){
    var _ = this;
    _.vec3D[0] = Math.cos(_.angleV) * Math.sin(_.angleH);
    _.vec3D[1] = Math.sin(_.angleV);
    _.vec3D[2] = Math.cos(_.angleV) * Math.cos(_.angleH);

    var halfPi = Math.PI/2.0;

    _.vec3R[0] = Math.sin(_.angleH-halfPi);
    _.vec3R[1] = 0;
    _.vec3R[2] = Math.cos(_.angleH-halfPi);

    vec3.cross(_.vec3U, _.vec3R, _.vec3D);
    vec3.add(_.vec3T, _.vec3P, _.vec3D);

    mat4.lookAt(_.matrixView, _.vec3P, _.vec3T, _.vec3U);
    //mat4.multiply(_.matrixView, _.matrixView,_.matrixRotate);


    _.updateMatrix();
};

Camera.prototype.updateMatrix = function(){
    mat4.invert(this.matrixView, this.matrixView);
    mat4.multiply(this.matrix, this.matrixProj, this.matrixView);
};

Camera.prototype.stepPositionForward = function(step){
    var _ = this;
    _.vec3D[0] *= step;
    _.vec3D[1] *= step;
    _.vec3D[2] *= step;
    vec3.add(_.vec3P,_.vec3P,_.vec3D);
};

Camera.prototype.stepPositionRight = function(step){
    var _ = this;
    _.vec3R[0] *= step;
    _.vec3R[1] *= step;
    _.vec3R[2] *= step;
    vec3.add(_.vec3P,_.vec3P,_.vec3R);
};

Camera.prototype.arcRotate = function(angleX, angleY, target){
    var _ = this;

    //var distance = 10;
    //
    //var p = Math.PI/180;
    //
    //var x = distance * -(Math.sin(angleX*p)) * Math.cos((angleY)*p);
    //var y = distance * -(Math.sin(angleY*p));
    //var z = -distance * Math.cos(angleX*p) * Math.cos(angleY*p);
    //
    //_.vec3D[0] = Math.cos(_.angleV) * Math.sin(_.angleH);
    //_.vec3D[1] = Math.sin(_.angleV);
    //_.vec3D[2] = Math.cos(_.angleV) * Math.cos(_.angleH);
    //
    //var halfPi = Math.PI/2.0;
    //
    //_.vec3R[0] = Math.sin(_.angleH-halfPi);
    //_.vec3R[1] = 0;
    //_.vec3R[2] = Math.cos(_.angleH-halfPi);
    //
    //vec3.cross(_.vec3U, _.vec3R, _.vec3D);
    //vec3.add(_.vec3T, [x,y,z], _.vec3D);
    //
    //mat4.lookAt(_.matrixView, [x,y,z], target, [0,1,0]);

    //console.log(_.matrixView);


    //var qP = quat.fromValues(_.vec3R[0],_.vec3R[1],_.vec3R[2],1.0);
    //var qP2 = quat.rotateX(quat.create(),qP,pitch);
    //
    //var pmt = vec3.subtract(vec3.create(), _.vec3P,target);
    //
    //var p1 = vec3.multiply(vec3.create(),qP2,pmt);
    //var p2 = vec3.add(vec3.create(),p1,target);
    //
    //_.vec3P = p2;
    //
    //var yP = quat.fromValues(0,1,0,1.0);
    //var yP2 = quat.rotateY(quat.create(),yP,yaw);
    //
    //var pmt = vec3.subtract(vec3.create(), _.vec3P,target);
    //
    //var p1 = vec3.multiply(vec3.create(),qP2,pmt);
    //var p2 = vec3.add(vec3.create(),p1,target);
    //
    //_.vec3P = p2;
    //_.stepPositionForward(0.05);
    //mat4.rotateX(_.matrixRotate, _.matrixRotate,pitch);
    //_.vec3P = [0,0,0];
    _.angleV += 0.005;
    _.angleH += 0.005;
    //_.vec3P = [0,8,-5];
    //_.stepPositionForward(-10);
    //mat4.lookAt(_.matrixView, _.vec3P,target, _.vec3U);
};

Camera.prototype.stepPositionUp = function(step){
    var _ = this;
    _.vec3U[0] *= step;
    _.vec3U[1] *= step;
    _.vec3U[2] *= step;
    vec3.add(_.vec3P,_.vec3P,_.vec3U);
};

Camera.prototype.getPosition = function(){
    return this.vec3P;
};

Camera.prototype.getMatrix = function(){
    return this.matrix;
};

//Camera.prototype.rotateAroundPoint = function(point,angle,axis){
//    var _ = this;
//    _.ver3D = point[0];
//    _.ver3D = point[1];
//    _.ver3D = point[2];
//
//    _.matrixRotate[12] = point[0];
//    _.matrixRotate[13] = point[1];
//    _.matrixRotate[14] = point[2];
//
//    mat4.rotate(_.matrixRotate, _.matrixRotate, angle,axis);
//};





