/**
 * Created by Ballas on 12/14/2015.
 */

var Shader = function(){
    var _ = this;

    _.program = null;
    _.vert = null;
    _.frag = null;

    _.attr = {};
    _.unif = {};
};

Shader.prototype.attachAttribute = function(attr,gl){
    var _ = this;
    gl.useProgram(_.program);
    _.attr[attr] = gl.getAttribLocation(_.program,attr);
};

Shader.prototype.attachUniform = function(unif,gl){
    var _ = this;
    gl.useProgram(_.program);
    _.unif[unif] = gl.getUniformLocation(_.program,unif);
};

Shader.prototype.enable = function(gl){
    var _ = this;
    gl.useProgram(_.program);
    for(var attr in _.attr){
        gl.enableVertexAttribArray(_.attr[attr]);
    }
};

Shader.prototype.disable = function(gl){

};

Shader.createShader = function(vert, frag, gl){

    var getShader = function(id){
        var shaderScript, theSource, currentChild, shader;
        shaderScript = document.getElementById(id);

        if (!shaderScript) {
            return null;
        }

        theSource = "";
        currentChild = shaderScript.firstChild;

        while(currentChild) {
            if (currentChild.nodeType == currentChild.TEXT_NODE) {
                theSource += currentChild.textContent;
            }

            currentChild = currentChild.nextSibling;
        }

        if (shaderScript.type == "x-shader/x-fragment") {
            shader = gl.createShader(gl.FRAGMENT_SHADER);
        } else if (shaderScript.type == "x-shader/x-vertex") {
            shader = gl.createShader(gl.VERTEX_SHADER);
        } else {
            // Unknown shader type
            return null;
        }

        gl.shaderSource(shader, theSource);

        // Compile the shader program
        gl.compileShader(shader);

        // See if it compiled successfully
        if (! gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            alert("An error occurred compiling the shaders: " + gl.getShaderInfoLog(shader) +" : "+id);
            return null;
        }
        return shader;
    };

    var vertShader = getShader(vert);
    var fragShader = getShader(frag);

    var shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertShader);
    gl.attachShader(shaderProgram, fragShader);
    gl.linkProgram(shaderProgram);

    // If creating the shader program failed, alert
    if (! gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert("Unable to initialize the shader program.");
    }

    var shader = new Shader();

    shader.program = shaderProgram;
    shader.vert = vertShader;
    shader.frag = fragShader;

    return shader;
};
